#include "../src/ModRM.h"

TEST(ModRM, Mod11) {
    char dest[] = "EAX"; 
    char src[]  = "EBX";
    EXPECT_EQ(getReg(src)  , 0b011);
    EXPECT_EQ(getReg(dest) , 0b000); 
    EXPECT_EQ(encodeModRM(dest, src), 0xD8);
}
TEST(ModRM, Mod00) {
    char dest[] = "EAX"; 
    char src[]  = "[EBX]";
    EXPECT_EQ(getReg(dest), 0b000);
    EXPECT_EQ(getRm(src)  , 0b011); 
    EXPECT_EQ(encodeModRM(dest, src), 0x03);
}
TEST(ModRM, Mod01) {
    char dest[] = "EAX"; 
    char src[]  = "[EDX]";
    int disp =  0x12;
    EXPECT_EQ(getReg(dest), 0b000);
    EXPECT_EQ(getRm(src)  , 0b010); 
    EXPECT_EQ(getMod(disp)  , 0b01); 
    EXPECT_EQ(encodeModRM(dest, src, disp), 0x42);
}
TEST(ModRM, Mod10) {
    char dest[] = "EAX"; 
    char src[]  = "[EDX]";
    int disp =  0x1234;
    EXPECT_EQ(getReg(dest), 0b000);
    EXPECT_EQ(getRm(src)  , 0b010); 
    EXPECT_EQ(getMod(disp) , 0b10); 
    EXPECT_EQ(encodeModRM(dest, src, disp), 0x82);
}
