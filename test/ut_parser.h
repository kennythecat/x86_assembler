#include "../src/parser.h"

TEST(Parser, ModRM_Reg2Reg) {
    const char* instruction = "mov eax, edx";
    inst my_inst = parser(instruction);
    EXPECT_STREQ(my_inst.opcode, "mov");
    EXPECT_STREQ(my_inst.dest,   "eax");
    EXPECT_STREQ(my_inst.src,    "edx");
}

TEST(Parser, ModRM_Reg2Mem) {
    const char* instruction = "mov [eax], edx";
    inst my_inst = parser(instruction);
    EXPECT_STREQ(my_inst.opcode, "mov");
    EXPECT_STREQ(my_inst.dest,   "[eax]");
    EXPECT_STREQ(my_inst.src,    "edx");
}

TEST(Parser, SIB_Reg2Reg) {
    const char* instruction = "mov eax, EBX+ECX*2";
    inst my_inst = parser(instruction);
    EXPECT_STREQ(my_inst.opcode, "mov");
    EXPECT_STREQ(my_inst.dest,   "eax");
    EXPECT_STREQ(my_inst.src,    "EBX");
    EXPECT_STREQ(my_inst.base,    "EBX");
    EXPECT_STREQ(my_inst.index,    "ECX");
    EXPECT_EQ(my_inst.scale,    2);
}