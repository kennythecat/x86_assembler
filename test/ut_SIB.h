#include "../src/SIB.h"

TEST(SIB, SS00) {
    char base[] = "EBX"; 
    char index[]= "ECX";
    int scale = 0;
    EXPECT_EQ(getBI(base) , 0b011);
    EXPECT_EQ(getBI(index), 0b001);
    EXPECT_EQ(getSS(scale), 0);
    EXPECT_EQ(encodeSIB(scale, index, base), 0x0b);
}

TEST(SIB, SS01) {
    char base[] = "EBX"; 
    char index[]= "ECX";
    int scale = 2;
    EXPECT_EQ(getBI(base) , 0b011);
    EXPECT_EQ(getBI(index), 0b001);
    EXPECT_EQ(getSS(scale), 0b01);
    EXPECT_EQ(encodeSIB(scale, index, base), 0x4b);
}

TEST(SIB, SS10) {
    char base[] = "EBX"; 
    char index[]= "ECX";
    int scale = 4;
    EXPECT_EQ(getBI(base) , 0b011);
    EXPECT_EQ(getBI(index), 0b001);
    EXPECT_EQ(getSS(scale), 0b10);
    EXPECT_EQ(encodeSIB(scale, index, base), 0x8b);
}

TEST(SIB, SS11) {
    char base[] = "EBX"; 
    char index[]= "ECX";
    int scale = 8;
    EXPECT_EQ(getBI(base) , 0b011);
    EXPECT_EQ(getBI(index), 0b001);
    EXPECT_EQ(getSS(scale), 0b11);
    EXPECT_EQ(encodeSIB(scale, index, base), 0xcb);
}

TEST(SIB, NoIndex) {
    char base[] = "ESP"; 
    char index[]= " ";
    int scale = 0;
    EXPECT_EQ(getBI(base) , 0b100);
    EXPECT_EQ(getBI(index), 0b100);
    EXPECT_EQ(getSS(scale), 0b00);
    EXPECT_EQ(encodeSIB(scale, index, base), 0b00100100);
}
TEST(SIB, NoBase) {
    char base[] = "*"; 
    char index[]= "ESI";
    int scale = 4;
    EXPECT_EQ(getBI(base) , 0b101);
    EXPECT_EQ(getBI(index), 0b110);
    EXPECT_EQ(getSS(scale), 0b10);
    EXPECT_EQ(encodeSIB(scale, index, base), 0b10110101);
}