#include "../src/parser.h"
#include "../src/ModRM.h"
#include "../src/SIB.h"

TEST(ALL, MODRM_Reg2Reg) {
    const char* instruction = "mov EAX, EBX";
    inst my_inst = parser(instruction);
    EXPECT_EQ(encodeModRM(my_inst.dest, my_inst.src), 0xD8);
}

TEST(ALL, MODRM_Reg2Mem) {
    const char* instruction = "mov EAX, [EBX]";
    inst my_inst = parser(instruction);
    EXPECT_EQ(encodeModRM(my_inst.dest, my_inst.src), 0x03);
}

TEST(ALL, SIB_Reg2Reg) {
    const char* instruction = "mov EAX, [EBX+ECX*2]";
    inst my_inst = parser(instruction);
    EXPECT_EQ(encodeModRM(my_inst.dest, my_inst.src), 0x03);
    EXPECT_EQ(encodeSIB(my_inst.scale, my_inst.index, my_inst.base), 0x4b);
}