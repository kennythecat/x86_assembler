.PHONY: clean test

all: directories ut_main

TEST: test/ut_ModRM.h test/ut_SIB.h test/ut_parser.h test/ut_all.h

MODRM: src/ModRM.h src/SIB.h src/getModRMCode/getReg.h src/getModRMCode/getMod.h \
	   src/getModRMCode/getMod.h

PARSER: src/parser.h

SRC: $(MODRM) $(PARSER)

ut_main: test/ut_main.cpp $(TEST) $(SRC)
	g++ -std=c++11 -Wfatal-errors test/ut_main.cpp -o bin/ut_all -lgtest -lpthread

directories:
	mkdir -p bin

clean:
	rm -rf bin
	
test: all
	bin/ut_all
