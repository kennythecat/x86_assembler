#ifndef SIB_H
#define SIB_H

#include <stdio.h>
#include <string.h>

int getBI(char* base) {
    if (strcmp(base, "EAX") == 0) return 0b000;
    if (strcmp(base, "ECX") == 0) return 0b001;
    if (strcmp(base, "EDX") == 0) return 0b010;
    if (strcmp(base, "EBX") == 0) return 0b011;
    if (strcmp(base, "ESP") == 0 || strcmp(base, " ") == 0) return 0b100;
    if (strcmp(base, "EBP") == 0 || strcmp(base, "*") == 0) return 0b101;
    if (strcmp(base, "ESI") == 0) return 0b110;
    if (strcmp(base, "EDI") == 0) return 0b111;
    return -1;
}

int getSS(int scale = 0) {
    if(scale == 2) return 0b01;
    else if(scale == 4) return 0b10;
    else if(scale == 8) return 0b11;
    else return 0b00;
}

int encodeSIB(int scale, char* index, char* base) {
    int S = getSS(scale);
    int I = getBI(index);
    int B = getBI(base);

    int SIB = (S << 6) | (I << 3) | B;
    return SIB;
}


#endif