#ifndef MODRM_H
#define MODRM_H

#include "getModRMCode/getReg.h"
#include "getModRMCode/getRM.h"
#include "getModRMCode/getMod.h"

int encodeModRM(char* dest, char* src, int disp = 0) {
    int REG = getReg(src);
    int RM  = getReg(dest);
    int MOD = 0b11;

    if (REG == -1) {
        MOD = getMod(disp);
        REG = getReg(dest);
        RM  = getRm(src);    
    }
    
    int modRM = (MOD << 6) | (REG << 3) | RM;
    return modRM;
}
#endif

