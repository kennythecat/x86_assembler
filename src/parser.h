#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include <string.h>

typedef struct INST{
    char* opcode;
    char* dest;
    char* src;
    char* base;
    char* index;
    int scale;
}inst;

inst parser(const char* instruction){
    char instrCopy[256];
    strncpy(instrCopy, instruction, sizeof(instrCopy));
    instrCopy[sizeof(instrCopy) - 1] = '\0';

    inst my_inst = {0};

    char* token = strtok(instrCopy, " ,+*");
    if (token) {
        my_inst.opcode = strdup(token);
        token = strtok(NULL, " ,+*");
    }
    if (token) {
        my_inst.dest = strdup(token);
        token = strtok(NULL, " ,+*");
    }
    if (token) {
        my_inst.src = strdup(token);
        token = strtok(NULL, " ,+*");
    }
//  
    if (token) {
        my_inst.index = strdup(token);
        token = strtok(NULL, " ,+*");
    }
    if (token) {
        my_inst.scale = strdup(token)[0] - '0';
    }
    my_inst.base = my_inst.src;
    return my_inst;
}

#endif