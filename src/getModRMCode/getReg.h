#ifndef GETREG_H
#define GETREG_H
#include <stdio.h>
#include <string.h>
// 32 bits

int getReg(char* reg) {
    if (strcmp(reg, "AL") == 0 || strcmp(reg, "AX") == 0 ||strcmp(reg, "EAX") == 0 ||\
        strcmp(reg, "MM0") == 0 || strcmp(reg, "XMM0") == 0 ) return 0b000;
    if (strcmp(reg, "CL") == 0 || strcmp(reg, "CX") == 0 ||strcmp(reg, "ECX") == 0 ||\
        strcmp(reg, "MM1") == 0 || strcmp(reg, "XMM1") == 0) return 0b001;
    if (strcmp(reg, "DL") == 0 || strcmp(reg, "DX") == 0 ||strcmp(reg, "EDX") == 0 ||\
        strcmp(reg, "MM2") == 0 || strcmp(reg, "XMM2") == 0) return 0b010;
    if (strcmp(reg, "BL") == 0 || strcmp(reg, "BX") == 0 ||strcmp(reg, "EBX") == 0 ||\
        strcmp(reg, "MM3") == 0 || strcmp(reg, "XMM3") == 0) return 0b011;
    if (strcmp(reg, "AH") == 0 || strcmp(reg, "SP") == 0 ||strcmp(reg, "ESP") == 0 ||\
        strcmp(reg, "MM4") == 0 || strcmp(reg, "XMM4") == 0) return 0b100;
    if (strcmp(reg, "CH") == 0 || strcmp(reg, "BP") == 0 ||strcmp(reg, "EBP") == 0 ||\
        strcmp(reg, "MM5") == 0 || strcmp(reg, "XMM5") == 0) return 0b101;
    if (strcmp(reg, "DH") == 0 || strcmp(reg, "SI") == 0 ||strcmp(reg, "ESI") == 0 ||\
        strcmp(reg, "MM6") == 0 || strcmp(reg, "XMM6") == 0) return 0b110;
    if (strcmp(reg, "BH") == 0 || strcmp(reg, "DI") == 0 ||strcmp(reg, "EDI") == 0 ||\
        strcmp(reg, "MM7") == 0 || strcmp(reg, "XMM7") == 0) return 0b111;
    return -1;
}

#endif