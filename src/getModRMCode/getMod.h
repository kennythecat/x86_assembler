#ifndef GETMOD_H
#define GETMOD_H
#include <stdio.h>
#include <string.h>

int getMod(int disp = 0) {
    if (disp == 0) return 0b00;
    if (disp >= -128 && disp <= 127) {
        return 0b01; 
    } else {
        return 0b10; 
    }
}
#endif