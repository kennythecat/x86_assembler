#ifndef GETRM_H
#define GETRM_H

#include <stdio.h>
#include <string.h>

int getRm(char* mem) {
    if (strcmp(mem, "[EAX]") == 0 || strcmp(mem, "EAX") == 0 || strcmp(mem, "AX") == 0 ||\
        strcmp(mem, "AL") == 0 || strcmp(mem, "MM0") == 0 || strcmp(mem, "XMM0") == 0) return 0b000;
    if (strcmp(mem, "[ECX]") == 0 || strcmp(mem, "ECX") == 0 || strcmp(mem, "CX") == 0 ||\
        strcmp(mem, "CL") == 0 || strcmp(mem, "MM1") == 0 || strcmp(mem, "XMM1") == 0) return 0b001;
    if (strcmp(mem, "[EDX]") == 0 || strcmp(mem, "EDX") == 0 || strcmp(mem, "DX") == 0 ||\
        strcmp(mem, "DDL") == 0 || strcmp(mem, "MM2") == 0 || strcmp(mem, "XMM2") == 0) return 0b010;
    if (strcmp(mem, "[EBX]") == 0 || strcmp(mem, "EBX") == 0 || strcmp(mem, "BX") == 0 ||\
        strcmp(mem, "BL") == 0 || strcmp(mem, "MM3") == 0 || strcmp(mem, "XMM3") == 0) return 0b011;
    if (strcmp(mem, "[--]") == 0 || strcmp(mem, "ESP") == 0 || strcmp(mem, "SP") == 0 ||\
        strcmp(mem, "AH") == 0 || strcmp(mem, "MM4") == 0 || strcmp(mem, "XMM4") == 0) return 0b100;
    if (strcmp(mem, "[EBP]") == 0 || strcmp(mem, "EBP") == 0 || strcmp(mem, "BP") == 0 ||\
        strcmp(mem, "CH") == 0 || strcmp(mem, "MM5") == 0 || strcmp(mem, "XMM5") == 0) return 0b101;
    if (strcmp(mem, "[ESI]") == 0 || strcmp(mem, "ESI") == 0 || strcmp(mem, "SI") == 0 ||\
        strcmp(mem, "DH") == 0 || strcmp(mem, "MM6") == 0 || strcmp(mem, "XMM6") == 0) return 0b110;
    if (strcmp(mem, "[EDI]") == 0 || strcmp(mem, "EDI") == 0 || strcmp(mem, "DI") == 0 ||\
        strcmp(mem, "BH") == 0 || strcmp(mem, "MM7") == 0 || strcmp(mem, "XMM7") == 0) return 0b111;
    return -1; 
}

#endif